/*
 */
package pixelsortinggui;

import GUIElements.JIntegerField;
import GUIElements.JRegexField;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author Oliver
 */
public class PixelSortingGUI extends JFrame{

	// TODO: the jar should be placed in the same folder as pixelsort.py, so make it relative
	private final String pixelSortPath = "D:\\My Documents\\NetBeansProjects\\PixelSortingGUI\\pixelsort\\pixelsort.py";

	// TODO: interval file should be an image, add a filefilter
	// TODO: input file should be an image, add a filefilter
	// TODO: file choosers should open to the current directory
	// TODO: drag-drop files as input
	private boolean requiresCharacteristic = false;
	private boolean requiresUpperThreshold = false;
	private boolean requiresIntervalFile = false;
	private boolean requiresThreshold = false;
	private String characteristicLength = "";
	private String intervalFunction = "";
	private String sortingFunction = "";
	private String thresholdUpper = "";
	private String thresholdLower = "";
	private String intervalFile = "";
	private String randomness = "";
	private String outputFile = "";
	private String inputFile = "";
	private String angle = "";


	/**
	 * Creates new form PixelSortingGUI
	 */
	public PixelSortingGUI(){
		initComponents();

		// hide the file chooser selection until its actually needed
		pnlFileIntervalChooser.setVisible(false);
		pnlThresholdUpper.setVisible(false);
		pnlCharacteristicLength.setVisible(false);
		// only allow numers between 0 and 1
		String floatRegex = "^(0(\\.\\d+)?|1(\\.0+)?)$";
		rgxThresholdLower.setRegexFilter(floatRegex);
		rgxThresholdUpper.setRegexFilter(floatRegex);

		// start all the button listeners
		initButtonListeners();

		// Ready to go!!
	}


	/**
	 * This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupInterval = new ButtonGroup();
        fileChooser = new JFileChooser();
        btnGroupSorting = new ButtonGroup();
        pnlInterval = new JPanel();
        radioIntervalRandom = new JRadioButton();
        radioIntervalEdges = new JRadioButton();
        radioIntervalThreshold = new JRadioButton();
        radioIntervalWave = new JRadioButton();
        radioIntervalFile = new JRadioButton();
        lblInterval = new JLabel();
        radioIntervalFileEdges = new JRadioButton();
        radioIntervalNone = new JRadioButton();
        pnlFileIntervalChooser = new JPanel();
        txtIntervalFile = new JTextField();
        lblIntervalFile = new JLabel();
        btnFileIntervalChooser = new JButton();
        btnStart = new JButton();
        jPanel1 = new JPanel();
        lblInputFile = new JLabel();
        txtInputFile = new JTextField();
        btnFileInputChooser = new JButton();
        pnlOutput = new JPanel();
        lblOutput = new JLabel();
        txtOutput = new JTextField();
        lblOutputSubtext = new JLabel();
        pnlThreshold = new JPanel();
        lblThreshold = new JLabel();
        lblThresholdLower = new JLabel();
        rgxThresholdLower = new JRegexField();
        pnlThresholdUpper = new JPanel();
        lblThresholdUpper = new JLabel();
        rgxThresholdUpper = new JRegexField();
        pnlCharacteristicLength = new JPanel();
        lblCharacteristicLength = new JLabel();
        intCharacteristicLength = new JIntegerField();
        pnlAngle = new JPanel();
        lblAngle = new JLabel();
        intAngle = new JIntegerField();
        pnlRandomness = new JPanel();
        lblRandomness = new JLabel();
        intRandomness = new JIntegerField();
        pnlSortingFunction = new JPanel();
        lblSorting = new JLabel();
        radioLightness = new JRadioButton();
        radioIntensity = new JRadioButton();
        radioMaximum = new JRadioButton();
        radioMinimum = new JRadioButton();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        btnGroupInterval.add(radioIntervalRandom);
        radioIntervalRandom.setText("Random");

        btnGroupInterval.add(radioIntervalEdges);
        radioIntervalEdges.setText("Edges");

        btnGroupInterval.add(radioIntervalThreshold);
        radioIntervalThreshold.setSelected(true);
        radioIntervalThreshold.setText("Threshold");

        btnGroupInterval.add(radioIntervalWave);
        radioIntervalWave.setText("Waves");

        btnGroupInterval.add(radioIntervalFile);
        radioIntervalFile.setText("File");

        lblInterval.setText("Interval");

        btnGroupInterval.add(radioIntervalFileEdges);
        radioIntervalFileEdges.setText("File-Edges");

        btnGroupInterval.add(radioIntervalNone);
        radioIntervalNone.setText("None");

        lblIntervalFile.setText("Interval File");
        lblIntervalFile.setToolTipText("");

        btnFileIntervalChooser.setText("...");

        GroupLayout pnlFileIntervalChooserLayout = new GroupLayout(pnlFileIntervalChooser);
        pnlFileIntervalChooser.setLayout(pnlFileIntervalChooserLayout);
        pnlFileIntervalChooserLayout.setHorizontalGroup(pnlFileIntervalChooserLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlFileIntervalChooserLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlFileIntervalChooserLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(lblIntervalFile)
                    .addGroup(pnlFileIntervalChooserLayout.createSequentialGroup()
                        .addComponent(txtIntervalFile, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnFileIntervalChooser, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))))
        );
        pnlFileIntervalChooserLayout.setVerticalGroup(pnlFileIntervalChooserLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlFileIntervalChooserLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblIntervalFile)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlFileIntervalChooserLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(txtIntervalFile, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFileIntervalChooser)))
        );

        GroupLayout pnlIntervalLayout = new GroupLayout(pnlInterval);
        pnlInterval.setLayout(pnlIntervalLayout);
        pnlIntervalLayout.setHorizontalGroup(pnlIntervalLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlIntervalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlIntervalLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(pnlIntervalLayout.createSequentialGroup()
                        .addGroup(pnlIntervalLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(pnlIntervalLayout.createSequentialGroup()
                                .addComponent(radioIntervalFile)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(pnlIntervalLayout.createSequentialGroup()
                                .addGroup(pnlIntervalLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(radioIntervalNone)
                                    .addComponent(radioIntervalFileEdges))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(pnlFileIntervalChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(pnlIntervalLayout.createSequentialGroup()
                        .addGroup(pnlIntervalLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(radioIntervalThreshold)
                            .addComponent(radioIntervalWave)
                            .addComponent(radioIntervalRandom)
                            .addComponent(radioIntervalEdges)
                            .addComponent(lblInterval))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        pnlIntervalLayout.setVerticalGroup(pnlIntervalLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlIntervalLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(lblInterval)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(radioIntervalRandom)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioIntervalEdges)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioIntervalThreshold)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioIntervalWave)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(radioIntervalFile)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlIntervalLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(pnlIntervalLayout.createSequentialGroup()
                        .addComponent(radioIntervalFileEdges)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(radioIntervalNone))
                    .addComponent(pnlFileIntervalChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btnStart.setText("Start");

        lblInputFile.setText("Input File");

        btnFileInputChooser.setText("...");

        lblOutput.setText("Output Filename");

        lblOutputSubtext.setFont(new Font("Tahoma", 0, 8)); // NOI18N
        lblOutputSubtext.setText("(leave blank for random)");

        GroupLayout pnlOutputLayout = new GroupLayout(pnlOutput);
        pnlOutput.setLayout(pnlOutputLayout);
        pnlOutputLayout.setHorizontalGroup(pnlOutputLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlOutputLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlOutputLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(txtOutput, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblOutputSubtext)
                    .addComponent(lblOutput))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlOutputLayout.setVerticalGroup(pnlOutputLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlOutputLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOutput)
                .addGap(1, 1, 1)
                .addComponent(lblOutputSubtext)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtOutput, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblInputFile)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtInputFile, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnFileInputChooser, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(pnlOutput, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblInputFile)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(txtInputFile, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFileInputChooser))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlOutput, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblThreshold.setLabelFor(pnlThreshold);
        lblThreshold.setText("Threshold");

        lblThresholdLower.setText("Lower");

        rgxThresholdLower.setText("0.25");
        rgxThresholdLower.setToolTipText("Enter any value between 0 and 1");

        lblThresholdUpper.setText("Upper");

        rgxThresholdUpper.setText("0.8");
        rgxThresholdUpper.setToolTipText("Enter any value between 0 and 1");

        GroupLayout pnlThresholdUpperLayout = new GroupLayout(pnlThresholdUpper);
        pnlThresholdUpper.setLayout(pnlThresholdUpperLayout);
        pnlThresholdUpperLayout.setHorizontalGroup(pnlThresholdUpperLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlThresholdUpperLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(lblThresholdUpper))
            .addComponent(rgxThresholdUpper, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
        );
        pnlThresholdUpperLayout.setVerticalGroup(pnlThresholdUpperLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlThresholdUpperLayout.createSequentialGroup()
                .addComponent(lblThresholdUpper)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rgxThresholdUpper, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        );

        GroupLayout pnlThresholdLayout = new GroupLayout(pnlThreshold);
        pnlThreshold.setLayout(pnlThresholdLayout);
        pnlThresholdLayout.setHorizontalGroup(pnlThresholdLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlThresholdLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlThresholdLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblThresholdLower)
                    .addComponent(lblThreshold, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(rgxThresholdLower, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlThresholdUpper, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlThresholdLayout.setVerticalGroup(pnlThresholdLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlThresholdLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlThresholdLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addComponent(pnlThresholdUpper, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlThresholdLayout.createSequentialGroup()
                        .addComponent(lblThreshold)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblThresholdLower)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rgxThresholdLower, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(81, Short.MAX_VALUE))
        );

        lblCharacteristicLength.setText("Characteristic Length");

        intCharacteristicLength.setText("50");

        GroupLayout pnlCharacteristicLengthLayout = new GroupLayout(pnlCharacteristicLength);
        pnlCharacteristicLength.setLayout(pnlCharacteristicLengthLayout);
        pnlCharacteristicLengthLayout.setHorizontalGroup(pnlCharacteristicLengthLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlCharacteristicLengthLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCharacteristicLengthLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addComponent(lblCharacteristicLength)
                    .addComponent(intCharacteristicLength, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlCharacteristicLengthLayout.setVerticalGroup(pnlCharacteristicLengthLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlCharacteristicLengthLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblCharacteristicLength)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(intCharacteristicLength, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblAngle.setText("Angle");

        intAngle.setText("0");
        intAngle.setToolTipText("Enter a number between 0 and 360");
        intAngle.setEnabled(false);

        GroupLayout pnlAngleLayout = new GroupLayout(pnlAngle);
        pnlAngle.setLayout(pnlAngleLayout);
        pnlAngleLayout.setHorizontalGroup(pnlAngleLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlAngleLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAngleLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblAngle, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(intAngle, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlAngleLayout.setVerticalGroup(pnlAngleLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlAngleLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAngle)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(intAngle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblRandomness.setText("Randomness");

        intRandomness.setText("0");
        intRandomness.setToolTipText("Enter a value between 0 and 100");

        GroupLayout pnlRandomnessLayout = new GroupLayout(pnlRandomness);
        pnlRandomness.setLayout(pnlRandomnessLayout);
        pnlRandomnessLayout.setHorizontalGroup(pnlRandomnessLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlRandomnessLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlRandomnessLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addComponent(lblRandomness)
                    .addComponent(intRandomness, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlRandomnessLayout.setVerticalGroup(pnlRandomnessLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlRandomnessLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblRandomness)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(intRandomness, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblSorting.setText("Sorting Function");

        btnGroupSorting.add(radioLightness);
        radioLightness.setSelected(true);
        radioLightness.setText("Lightness");

        btnGroupSorting.add(radioIntensity);
        radioIntensity.setText("Intensity");

        btnGroupSorting.add(radioMaximum);
        radioMaximum.setText("Maximum");

        btnGroupSorting.add(radioMinimum);
        radioMinimum.setText("Minimum");

        GroupLayout pnlSortingFunctionLayout = new GroupLayout(pnlSortingFunction);
        pnlSortingFunction.setLayout(pnlSortingFunctionLayout);
        pnlSortingFunctionLayout.setHorizontalGroup(pnlSortingFunctionLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlSortingFunctionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSortingFunctionLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(radioMinimum)
                    .addComponent(radioMaximum)
                    .addComponent(radioIntensity)
                    .addComponent(lblSorting)
                    .addComponent(radioLightness))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlSortingFunctionLayout.setVerticalGroup(pnlSortingFunctionLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pnlSortingFunctionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblSorting)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(radioLightness)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioIntensity)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioMaximum)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioMinimum)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlInterval, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnStart))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addComponent(pnlThreshold, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(52, 52, 52)
                                .addComponent(pnlRandomness, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(43, 43, 43)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(pnlCharacteristicLength, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(pnlAngle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pnlSortingFunction, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlThreshold, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(pnlInterval, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(pnlCharacteristicLength, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(pnlAngle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addComponent(pnlSortingFunction, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(pnlRandomness, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(17, 17, 17)
                .addComponent(btnStart)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]){
		/* Set the Windows look and feel */
		//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
		 * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
		 */
		try{
			for(UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()){
				if("Windows".equals(info.getName())){
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		}catch(ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex){
			Logger.getLogger(PixelSortingGUI.class.getName()).log(Level.SEVERE, null, ex);
		}
		//</editor-fold>

		//</editor-fold>

		/* Create and display the form */
		EventQueue.invokeLater(() -> {
			new PixelSortingGUI().setVisible(true);
		});
	}


	/**
	 * Keep all the button listeners in one place
	 */
	private void initButtonListeners(){
		btnStart.addActionListener(this::startButtonPressed);
		btnFileIntervalChooser.addActionListener(this::fileIntervalChooserPressed);
		btnFileInputChooser.addActionListener(this::fileInputChooserPressed);

		// add the same action listener for all radio buttons in the group
		// (so if an interval function is added or removed, it will still work)
		for(Enumeration<AbstractButton> buttons = btnGroupInterval.getElements(); buttons.hasMoreElements();){
			AbstractButton button = buttons.nextElement();
			button.addActionListener(this::radioIntervalSelected);
		}
	}


	/**
	 * If an interval function is selected which requires a file, show the file chooser panel
	 * otherwise hide it
	 * <p>
	 * @param event
	 */
	private void radioIntervalSelected(ActionEvent event){
		requiresCharacteristic = false;
		requiresUpperThreshold = false;
		requiresIntervalFile = false;
		requiresThreshold = false;

		JRadioButton source = (JRadioButton)event.getSource();
		if(source.equals(radioIntervalFile) || source.equals(radioIntervalFileEdges)){
			requiresIntervalFile = true;
		}
		if(source.equals(radioIntervalEdges)){
			requiresThreshold = true;
			requiresUpperThreshold = true;
		}
		if(source.equals(radioIntervalThreshold)){
			requiresThreshold = true;
		}
		if(source.equals(radioIntervalRandom)){
			requiresCharacteristic = true;
		}

		pnlCharacteristicLength.setVisible(requiresCharacteristic);
		pnlFileIntervalChooser.setVisible(requiresIntervalFile);
		pnlThresholdUpper.setVisible(requiresUpperThreshold);
		pnlThreshold.setVisible(requiresThreshold);
	}


	/**
	 * When the button next to the file path box is pressed
	 * <p>
	 * @param event
	 */
	private void fileIntervalChooserPressed(ActionEvent event){
		setTextAsChosenFile(txtIntervalFile);
	}


	/**
	 * When the button next to the input path box is pressed
	 * <p>
	 * @param event
	 */
	private void fileInputChooserPressed(ActionEvent event){
		setTextAsChosenFile(txtInputFile);
	}


	/**
	 * Sets the text of the given text field as the selected file from the file chooser
	 * <p>
	 * @param fileChooser
	 * @param textbox
	 */
	private void setTextAsChosenFile(JTextField textbox){
		int returnVal = fileChooser.showOpenDialog(this);

		if(returnVal == JFileChooser.APPROVE_OPTION){
			String tmp = fileChooser.getSelectedFile().getPath();
			textbox.setText(tmp);
		}
	}


	/**
	 * When the start button is pressed
	 * <p>
	 * @param event
	 */
	private void startButtonPressed(ActionEvent event){
		// get the text from textboxes here so we know we have the most up to date values
		intervalFunction = getSelectedButton(btnGroupInterval).getText().toLowerCase();
		intervalFile = txtIntervalFile.getText();
		inputFile = txtInputFile.getText();
		outputFile = txtOutput.getText();
		if(rgxThresholdLower.isTextValid()){
			thresholdLower = rgxThresholdLower.getText();
		}
		if(rgxThresholdUpper.isTextValid()){
			thresholdUpper = rgxThresholdUpper.getText();
		}
		characteristicLength = intCharacteristicLength.getText();
		angle = intAngle.getText();
		System.out.println(angle);
		randomness = intRandomness.getText();
		sortingFunction = getSelectedButton(btnGroupSorting).getText().toLowerCase();

		callPython();
	}


	private void callPython(){
		new Thread(() -> {
			try{
				Runtime rt = Runtime.getRuntime();
				List<String> c = new ArrayList<>();
				c.add("python3");
				c.add(pixelSortPath);
				c.add(inputFile);
				c.add("-i");
				c.add(intervalFunction);
				c.add("-s");
				c.add(sortingFunction);
				c.add("-r");
				c.add(randomness);
				if(!outputFile.isEmpty()){
					c.add("-o");
					c.add(outputFile);
				}
				if(requiresCharacteristic){
					c.add("-c");
					c.add(characteristicLength);
				}
				if(requiresIntervalFile){
					c.add("-f");
					c.add(intervalFile);
				}
				if(requiresThreshold){
					c.add("-t");
					c.add(thresholdLower);
				}
				if(requiresUpperThreshold){
					c.add("-u");
					c.add(thresholdUpper);
				}
				
				String[] commands = new String[c.size()];
				commands = c.toArray(commands);
				
				Process proc = rt.exec(commands);

				BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
				BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

				// read the output from the command
				System.out.println("Here is the standard output of the command:\n");
				String s;
				while((s = stdInput.readLine()) != null){
					System.out.println(s);
				}

				// read any errors from the attempted command
				System.out.println("Here is the standard error of the command (if any):\n");
				while((s = stdError.readLine()) != null){
					System.out.println(s);
				}
			}catch(IOException ex){
				Logger.getLogger(PixelSortingGUI.class.getName()).log(Level.SEVERE, null, ex);
			}
		}).start();
	}


	/**
	 * Gets the selected button in a given button group
	 * (if the group has toggle buttons and not radio buttons, it will return the first selected one)
	 * <p>
	 * @param btnGroup the group of buttons to find the selected button from
	 * <p>
	 * @return the selected button in the group
	 */
	private AbstractButton getSelectedButton(ButtonGroup btnGroup){
		for(Enumeration<AbstractButton> buttons = btnGroup.getElements(); buttons.hasMoreElements();){
			AbstractButton button = buttons.nextElement();
			if(button.isSelected()){
				return button;
			}
		}
		return null;
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnFileInputChooser;
    private JButton btnFileIntervalChooser;
    private ButtonGroup btnGroupInterval;
    private ButtonGroup btnGroupSorting;
    private JButton btnStart;
    private JFileChooser fileChooser;
    private JIntegerField intAngle;
    private JIntegerField intCharacteristicLength;
    private JIntegerField intRandomness;
    private JPanel jPanel1;
    private JLabel lblAngle;
    private JLabel lblCharacteristicLength;
    private JLabel lblInputFile;
    private JLabel lblInterval;
    private JLabel lblIntervalFile;
    private JLabel lblOutput;
    private JLabel lblOutputSubtext;
    private JLabel lblRandomness;
    private JLabel lblSorting;
    private JLabel lblThreshold;
    private JLabel lblThresholdLower;
    private JLabel lblThresholdUpper;
    private JPanel pnlAngle;
    private JPanel pnlCharacteristicLength;
    private JPanel pnlFileIntervalChooser;
    private JPanel pnlInterval;
    private JPanel pnlOutput;
    private JPanel pnlRandomness;
    private JPanel pnlSortingFunction;
    private JPanel pnlThreshold;
    private JPanel pnlThresholdUpper;
    private JRadioButton radioIntensity;
    private JRadioButton radioIntervalEdges;
    private JRadioButton radioIntervalFile;
    private JRadioButton radioIntervalFileEdges;
    private JRadioButton radioIntervalNone;
    private JRadioButton radioIntervalRandom;
    private JRadioButton radioIntervalThreshold;
    private JRadioButton radioIntervalWave;
    private JRadioButton radioLightness;
    private JRadioButton radioMaximum;
    private JRadioButton radioMinimum;
    private JRegexField rgxThresholdLower;
    private JRegexField rgxThresholdUpper;
    private JTextField txtInputFile;
    private JTextField txtIntervalFile;
    private JTextField txtOutput;
    // End of variables declaration//GEN-END:variables
}
